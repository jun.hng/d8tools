<?php

class UpdateExport
{
  public $could_not_load = FALSE;
  protected $node;
  protected $trnode;
  protected $data;
  protected $preserveNid = false;
  protected $connection = NULL;

  function loadData($jsonfile)
  {
    $this->data = json_decode(file_get_contents($jsonfile));
  }

  function nid()
  {
    // not yet implemented.
    return null;
  }

  function id()
  {
    return $this->data->dcr_id;
  }

  function old_id()
  {
    //drush_print('debug d7nid ' . (int) $this->data->nid);
    return (int) $this->data->dcr_id;
  }

  function loadPageById() {
  }

  /**
   * Import files (images) from the Drupal 7 site
   * @param int $d7fid
   */
  function importFile($d7fid, &$d8fid=null)
  {
  }

  function updateField($field, $d8_field=null)
  {
    global $nid_old_to_new;
    global $nid_new_to_old;

    if (!$d8_field) {
      $d8_field = $field;
    }

    if($this->data->language == "en") {
      $origLang = "en";
      $oppLang = "fr";
    }
    else {
      $origLang = "fr";
      $oppLang = "en";
    }

    switch ($field) {
      case 'field_something':
        break;
      default:
	drush_print('Switch default, no action taken.');
        break;
    } //End switch

  } // End updateField function.

  function save()
  {
    write_to_json();
  }

  function write_to_json($id, $data) {
    if (file_exists($this->id().'.json')) {
      if ($fp = fopen($this->id().'.json', 'w')) {
        echo "Write " . $this->id() .  ".json\n";
        fwrite($fp, json_encode($this->data, JSON_PRETTY_PRINT));
        fclose($fp);
      }
    } else {
      echo "file does not exist? where is it writing to? what folder is this?.\n";
    }
  }

}


