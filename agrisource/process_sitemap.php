<?php
require 'vendor/autoload.php';
error_reporting(E_ALL);

if (!isset($argv[1])) {
  echo "usage: " . $argv[0] . ' /path/to/sitemap.json' . "\n";
  echo "need to specify the path to sitemap.json.";
  die;
}

class AgrisourceJsonHelper {

  public static function getParentNodeId($parentElement) {
    return $parentElement[0]->{'@attribute'}->nodeId;
  }

  public static function popLastClassRef($selectorString) {
    $posEnd = strripos($selectorString, '->'); 
    $newSelectorString = substr($selectorString, 0, $posEnd + 1);
  }

}

$json_string=file_get_contents($argv[1]/*"/var/www/clients/client1/web38/web/d8tools/agrisource/scrape_agrisource.json"*/);
$json_obj = json_decode($json_string);

$news = $json_obj->SiteMap->English->Node->Node[0]->Node[0];
$find_ppl = $json_obj->SiteMap->English->Node->Node[0]->Node[1];
$branches = $json_obj->SiteMap->English->Node->Node[0]->Node[2];
$human_res = $json_obj->SiteMap->English->Node->Node[0]->Node[3];
$our_dept = $json_obj->SiteMap->English->Node->Node[0]->Node[4];
$news_first = $news->Node[0]->{'@attributes'}->nodeId;


$new_array = array();
$cnt=0;
$iCnt=0;
$iiCnt = 0;
$iiiCnt = 0;
$type = 'news';
foreach($json_obj->SiteMap->English->Node->Node[0]->Node as $json_node) {
  $json_node_fr = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt];
  $menuId = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->nodeId;
  $new_array[$menuId]['page_url']['en'] = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->landingPageUrl;
  $new_array[$menuId]['page_url']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt]->{'@attributes'}->landingPageUrl;
  $new_array[$menuId]['parent_page']['en'] = $json_obj->SiteMap->English->Node->Node[0]->{'@attributes'}->landingPageUrl;
  $new_array[$menuId]['parent_page']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->{'@attributes'}->landingPageUrl;
  $new_array[$menuId]['title']['en'] = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->title;
  $new_array[$menuId]['title']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt]->{'@attributes'}->title;
  $new_array[$menuId]['breadcrumb']['en'] = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->breadcrumb;
  $new_array[$menuId]['breadcrumb']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt]->{'@attributes'}->breadcrumb;
  $new_array[$menuId]['temptype'] = $type;
  $new_array[$menuId]['parent_menuid'] = $json_obj->SiteMap->English->Node->Node[0]->{'@attributes'}->nodeId;
  $new_array[$menuId]['menuid'] = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->nodeId;
  foreach ($json_node->Node as $json_inner_node) {
    $json_inner_node_fr = null;
    foreach ($json_node_fr->Node as $json_i_node_fr) {
      if ($json_inner_node->{'@attributes'}->nodeId == $json_i_node_fr->{'@attributes'}->nodeId) {
        $json_inner_node_fr = $json_i_node_fr;
      }
    }
    if ($cnt == 0) {
      $type = 'news';
    }
    if ($cnt == 1) {
      $type = 'find_people';
    }
    if ($cnt == 2) {
      $type = 'find_people';
    }
    if ($cnt == 3) {
      $type = 'branches';
    }
    if ($cnt == 3) {
      $type = 'hr';
    }
    if ($cnt == 4) {
      $type = 'our_dept';
    }
    $key = $json_inner_node->{'@attributes'}->nodeId;
    if (isset($json_inner_node->{'@attributes'}->landingPageUrl) && is_numeric($json_inner_node->{'@attributes'}->landingPageUrl)) {
      // Use the dc_id as a key (when possible);
      $key = $json_inner_node->{'@attributes'}->landingPageUrl;
    }
    $new_array[$key]['node_id'] = $json_inner_node->{'@attributes'}->nodeId;
    $new_array[$key]['parent_node_id'] = $json_node->{'@attributes'}->nodeId;
    $new_array[$key]['dc_id']['en'] = $json_inner_node->{'@attributes'}->landingPageUrl;
    $new_array[$key]['dc_id']['fr'] = $json_inner_node_fr->{'@attributes'}->landingPageUrl;
    $new_array[$key]['title']['en'] = $json_inner_node->{'@attributes'}->title;
    $new_array[$key]['title']['fr'] = $json_inner_node_fr->{'@attributes'}->title;
    $new_array[$key]['breadcrumb']['en'] = $json_inner_node->{'@attributes'}->breadcrumb;
    $new_array[$key]['breadcrumb']['fr'] = $json_inner_node_fr->{'@attributes'}->breadcrumb;
    $new_array[$key]['type'] = $type;
    if (isset($json_inner_node->Node)) {
      foreach ($json_inner_node->Node as $json_inner_inner_en) {
        $json_inner_inner_fr = NULL;
        foreach ($json_inner_node_fr->Node as $json_i_i_fr) {
          if (isset($json_i_i_fr->{'@attributes'}->nodeId)) {
            if ($json_i_i_fr->{'@attributes'}->nodeId == $json_inner_inner_en->{'@attributes'}->nodeId) {
              $json_inner_inner_fr = $json_i_i_fr;
            }
          }
        }
        _node_extract($json_inner_inner_en, $json_inner_inner_fr, $new_array, $type, null, $json_inner_node->{'@attributes'}->nodeId, $json_inner_node->{'@attributes'}->landingPageUrl);
        if (isset($json_inner_inner_en->Node)) {
          foreach ($json_inner_inner_en->Node as $json_i_i_inner_en) {
            $json_i_i_inner_fr = NULL;
            foreach ($json_inner_inner_fr->Node as $json_i_i_i_fr) {
              if (isset($json_i_i_i_fr->{'@attributes'}->nodeId)) {
                if ($json_i_i_i_fr->{'@attributes'}->nodeId == $json_i_i_inner_en->{'@attributes'}->nodeId) {
                  $json_i_i_inner_fr = $json_i_i_i_fr;
                }
              }
            }
            _node_extract($json_i_i_inner_en, $json_i_i_inner_fr, $new_array, $type, null, $json_inner_inner_en->{'@attributes'}->nodeId, $json_inner_inner_en->{'@attributes'}->landingPageUrl);
            $iiiCnt++;
          }
        }
        $iiCnt++;
      }
    }
    $iCnt++;
  }
  $cnt++;
}
echo "iCnt = $iCnt\n";
echo "iiCnt = $iiCnt\n";
echo "iiiCnt = $iiiCnt\n";

function _node_extract($json_en, $json_fr, &$data, $type, $levelNumber, $parent_node_id = null, $parent_dc_id = null) {
  $cnt=0;
  $iCnt=0;
  $topLevel = null;
  if (isset($json_en->{'@attributes'}->nodeId)) {
    $top_json_en = $json_en->{'@attributes'};
    $top_json_fr = $json_fr->{'@attributes'};
    if (!isset($data[$top_json_en->nodeId])) {
      $key = null;
      if (isset($top_json_en->landingPageUrl) && is_numeric($top_json_en->landingPageUrl)) {
        // Not all nodes on the sitemap have a dc_id (aka landingPageUrl).
        $key = $top_json_en->landingPageUrl;
      } else {
        // use the node_id as a key instead.
        $key = $top_json_en->nodeId;
      }
      if (!isset($data[$key])) {
        $data[$key]['node_id'] = $top_json_en->nodeId;
        $data[$key]['parent_node_id'] = $parent_node_id;
        $data[$key]['dc_id'] = $top_json_en->landingPageUrl;
        $data[$key]['landingPageUrl']['en'] = $top_json_en->landingPageUrl;
        $data[$key]['landingPageUrl']['fr'] = $top_json_fr->landingPageUrl;
        $data[$key]['parent_dc_id'] = $parent_dc_id;
        $data[$key]['node_title']['en'] = $top_json_en->title;
        $data[$key]['node_title']['fr'] = $top_json_fr->title;
        $data[$key]['title']['en'] = $top_json_en->breadcrumb;
        $data[$key]['title']['fr'] = $top_json_fr->breadcrumb;
        $data[$key]['type'] = $type;
      }
    }
  }
}

$dir = 'export';
if (!is_dir($dir)) {
  mkdir($dir);
}
chdir($dir);
$keep_existing = false;
$jsonCnt = 0;
foreach($new_array as $key => $entity) {
  if ($keep_existing) {
    if (file_exists($key.'.json')) {
      continue;
    }
  }
  if ($fp = fopen($key.'.json', 'w')) {
    //echo "Write $key.json\n";
    $jsonCnt++;
    fwrite($fp, json_encode($entity, JSON_PRETTY_PRINT));
    fclose($fp);
  }
}
echo "Wrote $jsonCnt json files to $dir\n";
chdir('..');

//$test = json_encode($new_array, JSON_PRETTY_PRINT);
//echo $test;

//echo $test;
die;

