<?php
include "updaters/UpdateExport.php";

global $argv;
global $export_root;
//global $nid_old_to_new;
//global $nid_new_to_old;
global $site_dir;
//global $db;
global $d7_nids;

$site_dir = 'default';
//$nid_old_to_new = array();
//$nid_new_to_old = array();
$d7_nids = array();

$myargs = array_slice($argv, 3);

$updateTypes = array('teamsite'); //Removed 'page' it is now d8 update instead

// Override when testing
// When testing and you just want to test one content type, use the option for the type and it will update only this..

if (empty($myargs)) {
  echo "Syntax: php update_json.php full-path-containing-exports teamsite\n";
  echo "When testing and you just want to test one content type, use the option for the type and it will update only this..\n";
  exit;
}

$update_only_option_type = false;
$export_root = array_shift($myargs);
$option = array_shift($myargs);
echo "export_root: $export_root \n";

if (!empty($option)) {
  echo "update option: $option \n";
  if (strlen($option) > 0) {
    $update_only_option_type = true;
    $updateTypes = array($option);
  }
}
chdir($export_root);

/*if ($result = $db->query("SELECT d7_nid FROM {node}")) {
  while ($row = $result->fetchObject()) {
    $d7_nids[] = $row->d7_nid;
  }
}*/

// Now update each content type
foreach ($updateTypes as $type) {
  if ($type == $option && $update_only_option_type) {
    update_ctype($type);
  }
  else if (!$update_only_option_type) {
    // Do all types.
    update_ctype($type);
  }
}

function update_ctype($type) {
//  global $nid_old_to_new;
//  global $nid_new_to_old;
//  global $db;
  global $d7_nids;

  echo "getcwd()= " . getcwd() . ";\n";
  echo "chdir('" . $type . "');\n";
  //chdir($type);
  $jsonfiles = array();

  if ($dirh = opendir(".")) {
    while (($entry = readdir($dirh)) !== false) {
      if (!preg_match('/json$/', $entry)) continue;
      $jsonfiles[] = $entry;
      if (strtolower($type) == 'teamsite') {
        echo "json file $entry\n";
      }
    }
    closedir($dirh);
  }

  $cclass = ucfirst($type);
  //include "updaters/$cclass.php";
  include "updaters/teamsite.php";

  echo "Processing ".count($jsonfiles)." $type entities...\n";
  $cnt = 0;
  $cnt_success = 0;
  $cnt_could_not_load = 0;
  foreach ($jsonfiles as $jfile) {
    $d7_nid = null;
    if (preg_match('/(\d+)\.json$/', $jfile, $matches)) {
      $d7_nid = $matches[1];
      echo $matches[1] . '.json' . "\n";
      $d7_nid = ltrim($d7_nid, '0');
    }
    /*if ($d7_nid && in_array($d7_nid, $d7_nids)) {
      echo "Found d7_nid: $d7_nid mapping in D8 \n";
      //continue;
    }*/
    $cnt++;
    //echo "$cnt\n";
    if (($cnt % 100) == 0) {
      echo "$cnt\n";
    }
    /*$update = new $cclass();
    $update->update($jfile);
    if ($update->success) {
      $cnt_success++;
    }
    if ($update->could_not_load) {
      $cnt_could_not_load++;
    }*/
  }
  echo "\n";
  echo "$cnt_success nodes were successfully updated with twitter override images\n";
  echo "$cnt_could_not_load d7nid did not resolve to d8nid, unable to load $cnt_could_not_load times.\n";
  echo "$cnt json files were processed\n";
  chdir('..');
}

