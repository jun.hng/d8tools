<?php


if (!isset($argv[1])) {
  echo "usage: " . $argv[0] . ' filename.csv \t' . "\n";
  //echo "assumes header as follows: DCR_ID   LANG   TITLE   MODIFIED   ISSUED   TYPE_NAME   NODE_ID   LAYOUT_NAME   CREATED   UPDATED\n";
  echo "assumes header as follows: dcr_id   lang   title   modified   issued   type_name   node_id   layout_name   created   updated\n";
  die;
}

$file = file_get_contents($argv[1]);
$csv = csv_to_array($argv[1]);
function csv_to_array($filename='', $delimiter="\t")
{
  if(!file_exists($filename) || !is_readable($filename))
    return FALSE;
	
    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE)
    {
      while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
      {
        if(!$header)
          $header = $row;
        else
          $data[] = array_combine($header, $row);
      }
      fclose($handle);
    }
    return $data;
}

$array_of_content = array();
foreach($csv as $row) {
  if ($row['layout_name'] == '<NULL>') {
    $row['layout_name'] = NULL;
  }
  if ($row['lang'] == 'eng') {
    $row['lang'] = 'en';
  }
  if ($row['lang'] == 'fra') {
    $row['lang'] = 'fr';
  }
  if (!isset($array_of_content[$row['dcr_id']])) {
    $array_of_content[$row['dcr_id']] = $row;
  } else {
    $origRow = $array_of_content[$row['dcr_id']];
    if ($origRow['lang'] == 'en' && !isset($row['TITLE']['en']) && $row['lang'] == 'fr') {
      $titleEn = $origRow['title'];
      $origRow['title'] = array();
      $origRow['title']['en'] = $titleEn;
      $origRow['title']['fr'] = $row['title'];
      $array_of_content[$origRow['dcr_id']] = $origRow;
    }
  }
}

$objs = arrayToObject($array_of_content);
function arrayToObject($array) {
    if (!is_array($array)) {
        return $array;
    }
    
    $object = new stdClass();
    if (is_array($array) && count($array) > 0) {
        foreach ($array as $name=>$value) {
            $name = strtolower(trim($name));
            if (!empty($name)) {
                $object->$name = arrayToObject($value);
            }
        }
        return $object;
    }
    else {
        return FALSE;
    }
}

//echo print_r($objs, TRUE);

$dir = 'new_export';
if (!is_dir($dir)) {
  mkdir($dir);
}
chdir($dir);
$keep_existing = true;
foreach ($array_of_content as $key => $entity) {
  if ($keep_existing) {
    if (file_exists($key.'.json')) {
      continue;
    }
  }
  if ($fp = fopen($key.'.json', 'w')) {
    $entity_obj = arrayToObject($entity);
    echo "Write $key.json\n";
    fwrite($fp, json_encode($entity, JSON_PRETTY_PRINT));
    fclose($fp);
  }
}
